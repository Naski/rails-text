Rails.application.routes.draw do
  get 'main/index'
  root 'main#index'
  get 'homepage', to: 'homepage#index'
  get 'new', to: 'main#new'
  post '/zoom' => 'main#zoomcreate'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end

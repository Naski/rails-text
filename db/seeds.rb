# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

zoom = Zoom.create(title: 'Trend and Market Research', meetid: '82231642363', meetpwd: 'TjU2NFJCVkpiRFVlaEJSdzF1ZTROUT09', prof: 'Monika Engelen', desc: 'Zoomraum für den Kurs Trend and Market Research', times: 0)
zoom2 = Zoom.create(title: 'Hello World', meetid: '82231642363', meetpwd: 'TjU2NFJCVkpiRFVlaEJSdzF1ZTROUT09', prof: 'Monika Engelen', desc: 'Zoomraum für den Kurs Hello World', times: 0)
zoom3 = Zoom.create(title: 'Neue Welt', meetid: '82231642363', meetpwd: 'TjU2NFJCVkpiRFVlaEJSdzF1ZTROUT09', prof: 'Monika Engelen', desc: 'Zoomraum für den Kurs Neue Welt', times: 0)
zoom4 = Zoom.create(title: 'Krasser Kurs', meetid: '82231642363', meetpwd: 'TjU2NFJCVkpiRFVlaEJSdzF1ZTROUT09', prof: 'Monika Engelen', desc: 'Zoomraum für den Kurs Krasser Kurs', times: 0)
zoom5 = Zoom.create(title: 'Professionelles Schauspielern', meetid: '82231642363', meetpwd: 'TjU2NFJCVkpiRFVlaEJSdzF1ZTROUT09', prof: 'Monika Engelen', desc: 'Zoomraum für den Kurs Professionelles Schauspielern', times: 0)

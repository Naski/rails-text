class CreateEntries < ActiveRecord::Migration[6.1]
  def change
    create_table :entries do |t|
      t.string :title
      t.string :link
      t.string :meetid
      t.string :meetpwd
      t.string :prof
      t.string :desc
      t.integer :times
      t.string :type

      t.timestamps
    end
  end
end

class MainController < ApplicationController
  skip_before_action :verify_authenticity_token
  def index
    @zoomentries = Entry.where(type: 'Zoom').order("created_at DESC")

  end

  def zoomcreate
    #zoom = Zoom.create(title: params.title, meetId: params.meetid)
    @title = nil
    #@meetid = params[":meetid"]
    #@meetpwd = params[":meetpwd"]
    #@prof = params[":prof"]
    #@desc = params[":desc"]
    @values = params[:entry]
    
    if @values.nil?
      return "Cannot create entry, form is not parsed correctly"
    else
      if Zoom.where(title: @values['title']).empty?
        Zoom.create(title: @values['title'], meetid: @values['meetid'], meetpwd: @values['meetpwd'], prof: @values['prof'], desc: @values['desc'], times: 0)
      end
      redirect_to '/'
    end
  end
end
